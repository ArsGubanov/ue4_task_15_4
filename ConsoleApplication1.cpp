﻿
#include <iostream>

void EvenOdd(int Parameter, int randomNumber)
{
    switch (Parameter)
    {

    case 1:
        std::cout << "Odd numbers from 0 to " << randomNumber << " are: \n";
        for (int i = 1; i <= randomNumber; i += 2)
        {
            std::cout << i << "\n";
        }
        break;

    case 2:
        std::cout << "Even numbers from 0 to " << randomNumber << " are: \n";
        for (int i = 0; i <= randomNumber; i += 2)
        {
            std::cout << i << "\n";
        }
        break;

    default:
        std::cout << "You must enter either '1' or '2'!";
    }
    

}

int main()
{
//Задача 1
    const int cNumber = 16;    

    std::cout << "Even numbers from 0 to " << cNumber << " are: \n";
    for (int i = 0; i <= cNumber; i += 2)
    {
        std::cout << i << "\n";
    }


//Задача 2

    std::cout << "\nPlease, enter a positive number: ";
    int inrandomNumber;
    std::cin >> inrandomNumber;
    std::cout << "Please, enter '1' to show odd numbers or '2' to show even ones: ";
    int inParameter;
    std::cin >> inParameter;
    EvenOdd(inParameter, inrandomNumber);

 


    return 0;

}

